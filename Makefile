CC = clang
COLLEEN = Colleen
GRACE = Grace
SULLY = Sully
CFILE1= Colleen.c
CFILE2 = Grace.c
CFILES3 = Sully.c

OFILES1 = Colleen.o
OFILES2 = Grace.o
OFILES3 = Sully.o

.PHONY: all clean re fclean

all: $(COLLEEN) $(GRACE) $(SULLY)

$(COLLEEN): $(OFILES1)
	$(CC) -Wall -Wextra -Werror Colleen.o -o $(COLLEEN)
$(GRACE): $(OFILES2)
	$(CC) -Wall -Wextra -Werror Grace.o -o $(GRACE)
$(SULLY): $(OFILES3)
	$(CC) -Wall -Wextra -Werror Sully.o -o $(SULLY)

$(OFILES1): $(CFILES1)
	$(CC) -Wall -Wextra -Werror Colleen.c -c
$(OFILES2): $(CFILES2)
	$(CC) -Wall -Wextra -Werror Grace.c -c
$(OFILES3): $(CFILES3)
	$(CC) -Wall -Wextra -Werror Sully.c -c

clean:
	rm -rf Colleen.o
	rm -rf Grace.o
	rm -rf Sully.o

fclean: clean
	rm -rf Colleen
	rm -rf Grace
	rm -rf Sully

re: fclean all